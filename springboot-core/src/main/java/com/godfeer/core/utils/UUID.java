package com.godfeer.core.utils;

class UUID {
	static String getUUID(){
		return java.util.UUID.randomUUID().toString().replaceAll("-", "").trim();
	}
}
