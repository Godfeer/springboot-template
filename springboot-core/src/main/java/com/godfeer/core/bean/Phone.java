package com.godfeer.core.bean;


import java.io.Serializable;

/**
 * 手机验证码实体类
 */

public class Phone implements Serializable{


    /**
     * IP
     */
    private String IP;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 验证码
     */
    private String verification;
    /**
     * 获取验证码次数
     */
    private int count = 0;

    /**
     *延时开关
     */
    private boolean timeBoolean;

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isTimeBoolean() {
        return timeBoolean;
    }

    public void setTimeBoolean(boolean timeBoolean) {
        this.timeBoolean = timeBoolean;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "IP='" + IP + '\'' +
                ", phone='" + phone + '\'' +
                ", verification='" + verification + '\'' +
                ", count=" + count +
                ", timeBoolean=" + timeBoolean +
                '}';
    }
}
