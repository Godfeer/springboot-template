package com.godfeer.core.my.myenum;

/**
 * 查询情况
 */
public enum SelectEnum {

    EMPTY("暂无内容！"),
    SUCCESS("查询成功！"),
    DEFAULT("查询失败!");


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    private SelectEnum(String message){
        this.message = message;
    }



}
