package com.godfeer.core.my.myconfig.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * cosClient的属性配置
 */
@Configuration
@ConfigurationProperties(prefix = COSClientProperties.PREFIX)
public class COSClientProperties {

    public static final String PREFIX = "cos.cosClient";

//    String accessKey = "AKIDxpW5rWULWXO4f8wccQ3U0SkqnbHqtTWQ";//secretId
//    String secretKey = "tb8UfeS4VkqmaGlDbx76uXyEUaXLySmx";//secretKey
//    String region_name = "ap-beijing";//region_name



    String accessKey ;//secretId
    String secretKey;//secretKey
    String region_name;//region_name



    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    @Override
    public String toString() {
        return "COSClientProperties{" +
                "accessKey='" + accessKey + '\'' +
                ", secretKey='" + secretKey + '\'' +
                ", region_name='" + region_name + '\'' +
                '}';
    }
}
