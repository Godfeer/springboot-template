package com.godfeer.core.my.myenum;

/**
 * 用户注册枚举
 */
public enum RegEnum {

    SUCCESS("注册成功！"),
    DEFAULT("注册失败！"),
    REGISTERED("已注册！");

    private String message;

    private RegEnum(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
