package com.godfeer.core.my.myenum;

/**
 * 用户登录枚举
 */
public enum LoginEnum {

    SUCCESS("登录成功！"),
    DEFAULT("登录失败！"),
    LOGINED("已经登录！"),
    OTHERLOGIN("异地登录！"),
    INPUTISNULL("请输入用户名和密码！"),
    LOGINOUT("用户退出！"),
    OUTDEFAULT("用户退出失败！");

    String message;


    LoginEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
