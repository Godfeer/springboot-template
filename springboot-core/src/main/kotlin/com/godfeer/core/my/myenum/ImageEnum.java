package com.godfeer.core.my.myenum;

/**
 *图片类型枚举
 */
public enum ImageEnum {
    PART(1,"党建教育"),JOB(2,"职业教育"),HIGH(3,"高等教育"),K12(4,"K12教育");

    int id;

    String type;

    ImageEnum(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
