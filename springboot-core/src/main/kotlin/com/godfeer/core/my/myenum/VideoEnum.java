package com.godfeer.core.my.myenum;

/**
 * 视频类型枚举
 */
public enum VideoEnum {
    TWO(1,"2D"),THREE(2,"3D"),VR(3,"VR");

    int id;

    String type;

    VideoEnum(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
