package com.godfeer.core.base.controller

import org.springframework.stereotype.Component
import org.springframework.web.servlet.View

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * 错误页面的默认跳转(例如请求404的时候,默认走这个视图解析器)
 *
 * @author fengshuonan
 * @date 2017-05-21 11:34
 */
@Component("error")
class BaseErrorView : View {

    override fun getContentType(): String {
        return "text/html"
    }

    @Throws(Exception::class)
    override fun render(map: MutableMap<String, *>?, httpServletRequest: HttpServletRequest, httpServletResponse: HttpServletResponse) {
        httpServletRequest.getRequestDispatcher("/global/error").forward(httpServletRequest, httpServletResponse)
    }
}
