package com.godfeer.core.base.tips;

/**
 * 返回给前台的错误提示 （错误json封装）
 */
public class Error extends Tip{

    public Error(){
        super.code = 100;
        super.message = "操作失败";
        super.data = "";
    }
}
