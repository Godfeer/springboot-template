package com.godfeer.core.base.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.godfeer.core.base.tips.ErrorTip;
import com.godfeer.core.base.tips.SuccessTip;
import com.godfeer.core.base.warpper.BaseControllerWarpper;
import com.godfeer.core.page.PageInfoBT;
import com.godfeer.core.support.HttpKit;
import com.godfeer.core.util.FileUtil;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;

/**
 * @author : godfeer@aliyun.com 
 * @date : 2018/6/26/026 
 **/

public class BaseController {

    //日志打印
    protected Logger log = Logger.getLogger(this.getClass());

    protected static String SUCCESS = "SUCCESS";
    protected static String ERROR = "ERROR";

    protected static String REDIRECT = "redirect:";
    protected static String FORWARD = "forward:";

    protected  static int COUNT = 0;
    protected  static int FOUR = 4;


    protected static final String PHONE_PREFIX = "subor_session_phone_";

    protected static final String SESSION_ACCOUBT_PREFFIX = "subor_session_account_";


    protected static SuccessTip SUCCESS_TIP = new SuccessTip();
    protected static ErrorTip ERROR_TIP = new ErrorTip(100,"");

    //COS文件路径
    protected static final  String PATH = "/";


    protected HttpServletRequest getHttpServletRequest() {
        return HttpKit.getRequest();
    }

    protected HttpServletResponse getHttpServletResponse() {
        return HttpKit.getResponse();
    }

    protected HttpSession getSession() {
        return HttpKit.getRequest().getSession();
    }

    protected HttpSession getSession(Boolean flag) {
        return HttpKit.getRequest().getSession(flag);
    }

    protected String getPara(String name) {
        return HttpKit.getRequest().getParameter(name);
    }

    protected void setAttr(String name, Object value) {
        HttpKit.getRequest().setAttribute(name, value);
    }

    protected Integer getSystemInvokCount() {
        return (Integer) this.getHttpServletRequest().getServletContext().getAttribute("systemCount");
    }

    /**
     * 把service层的分页信息，封装为bootstrap table通用的分页封装
     */
    protected <T> PageInfoBT<T> packForBT(Page<T> page) {
        return new PageInfoBT<T>(page);
    }

    /**
     * 包装一个list，让list增加额外属性
     */
    protected Object warpObject(BaseControllerWarpper warpper) {
        return warpper.warp();
    }

    /**
     * 删除cookie
     */
    protected void deleteCookieByName(String cookieName) {
        Cookie[] cookies = this.getHttpServletRequest().getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(cookieName)) {
                Cookie temp = new Cookie(cookie.getName(), "");
                temp.setMaxAge(0);
                this.getHttpServletResponse().addCookie(temp);
            }
        }
    }

    /**
     * 返回前台文件流
     *
     * @author fengshuonan
     * @date 2017年2月28日 下午2:53:19
     */
    protected ResponseEntity<byte[]> renderFile(String fileName, String filePath) {
        byte[] bytes = FileUtil.toByteArray(filePath);
        return renderFile(fileName, bytes);
    }

    /**
     * 返回前台文件流
     *
     * @author fengshuonan
     * @date 2017年2月28日 下午2:53:19
     */
    protected ResponseEntity<byte[]> renderFile(String fileName, byte[] fileBytes) {
        String dfileName = null;
        try {
            dfileName = new String(fileName.getBytes("gb2312"), "iso8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", dfileName);
        return new ResponseEntity<byte[]>(fileBytes, headers, HttpStatus.CREATED);
    }


    //设置成功返回参数
    public SuccessTip getSuccessTip(String message,Object data){
        SuccessTip successTip = new SuccessTip();
        successTip.setData(data);
        successTip.setMessage(message);
        return successTip;
    }
    //设置失败返回参数
    public ErrorTip getErrorTip(String message,Object data){
        ErrorTip errorTip = new ErrorTip(100,"");
        errorTip.setData(data);
        errorTip.setMessage(message);
        return errorTip;
    }

}
