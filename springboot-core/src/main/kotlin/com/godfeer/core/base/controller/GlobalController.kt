package com.godfeer.core.base.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping

/**
 * 全局的控制器
 *
 * @author fengshuonan
 * @date 2016年11月13日 下午11:04:45
 */
@Controller
@RequestMapping("/global")
class GlobalController {

    /**
     * 跳转到404页面
     *
     * @author fengshuonan
     */
    @RequestMapping(path = arrayOf("/error"))
    fun errorPage(): String {
        return "/404.html"
    }

    /**
     * 跳转到session超时页面
     *
     * @author fengshuonan
     */
    @RequestMapping(path = arrayOf("/sessionError"))
    fun errorPageInfo(model: Model): String {
        model.addAttribute("tips", "session超时")
        return "/login.html"
    }
}
