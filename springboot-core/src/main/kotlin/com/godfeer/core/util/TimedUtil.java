package com.godfeer.core.util;
import com.godfeer.core.base.controller.BaseController;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 定时任务
 */
public class TimedUtil extends BaseController {

    public static void Timer1(){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
                    @Override
                    public void run() {}
        }, 5000);// 设定指定的时间time,此处为5000毫秒
    }

}
