package com.godfeer.core.aop

import com.godfeer.core.base.tips.ErrorTip
import com.godfeer.core.exception.BaseException
import com.godfeer.core.exception.BaseExceptionEnum
import org.apache.log4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * 全局的的异常拦截器（拦截所有的控制器）（带有@RequestMapping注解的方法上都会拦截）
 *
 * @author godfeer
 * @date 2016年11月12日 下午3:19:56
 */
class BaseControllerExceptionHandler {

    private val log = Logger.getLogger(this.javaClass)

    /**
     * 拦截业务异常
     */
    @ExceptionHandler(BaseException::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    fun notFount(e: BaseException): ErrorTip {
        log.error("业务异常:", e)
        return ErrorTip(e.code!!, e.message!!)
    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    fun notFount(e: RuntimeException): ErrorTip {
        log.error("运行时异常:", e)
        return ErrorTip(BaseExceptionEnum.SERVER_ERROR.code!!, BaseExceptionEnum.SERVER_ERROR.message)
    }

}
