package com.godfeer.core.exception

/**
 * 封装预定义的异常
 * @author : godfeer@aliyun.com
 * @date : 2018/6/20/020
 */

class BaseException(serviceExceptionEnum: ServiceExceptionEnum) : RuntimeException() {

    var code: Int? = null

    private var msg: String

    init {
        this.code = serviceExceptionEnum.code
        this.msg = serviceExceptionEnum.message
    }

     fun getMsg(): String {
        return msg
    }

     fun setMsg(message: String) {
        this.msg = message
    }
}
