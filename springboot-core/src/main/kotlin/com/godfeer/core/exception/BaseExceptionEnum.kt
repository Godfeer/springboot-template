package com.godfeer.core.exception

/**
 * subor异常枚举
 *
 * @author fengshuonan
 * @Date 2017/12/28 下午10:33
 */
enum class BaseExceptionEnum private constructor(override var code: Int?, override var message: String) : ServiceExceptionEnum {

    /**
     * 其他
     */
    WRITE_ERROR(500, "渲染界面错误"),

    /**
     * 文件上传
     */
    FILE_READING_ERROR(400, "FILE_READING_ERROR!"),
    FILE_NOT_FOUND(400, "FILE_NOT_FOUND!"),

    /**
     * 错误的请求
     */
    REQUEST_NULL(400, "请求有错误"),
    SERVER_ERROR(500, "服务器异常")
}
