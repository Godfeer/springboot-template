package com.godfeer.core.exception

/**
 * 抽象接口
 * @author : godfeer@aliyun.com
 * @date : 2018/6/20/020
 */

interface ServiceExceptionEnum {

    /**
     * 获取异常编码
     */
    val code: Int?

    /**
     * 获取异常信息
     */
    val message: String
}
