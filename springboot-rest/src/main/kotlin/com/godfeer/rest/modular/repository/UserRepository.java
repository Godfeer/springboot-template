package com.godfeer.rest.modular.repository;

import com.godfeer.rest.modular.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/***
 * @author godfeer@aliyun.com
 */
public interface UserRepository extends JpaRepository<User,Long> {

    /**
     * 查询用户名称包含username字符串的用户对象
     * @param username 用户名
     * @return User
     */
    List<User> findByUsernameContaining(String username);

    /**
     * 获得单个用户对象，根据username和pwd的字段匹配
     * @param username 用户名
     * @param pwd 密码
     * @return User
     */
    User getByUsernameIsAndPasswordIs(String username, String pwd);

    /***
     * 精确匹配username的用户对象
     * @param username  用户名
     * @return User
     */
    User findByUsername(String username);
    /***
     * 精确匹配username的用户对象
     * @param phone  用户名
     * @return User
     */
    User findByPhone(String phone);
    /**
     * 获得单个用户对象，根据username和pwd的字段匹配
     * @param phone 手机号
     * @param password 密码
     * @return User
     */
    User getByPhoneIsAndPasswordIs(String phone, String password);

}