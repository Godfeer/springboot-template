package com.godfeer.rest.modular.repository;

import com.godfeer.rest.modular.model.file.FileInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author : godfeer@aliyun.com 
 * @date : 2018/6/22/022
 **/
public interface FileRepository extends JpaRepository<FileInfo,Long> {
    /**
     *
     * <p>Title: findByFileMd5</p>
     * <p>Description:根据文件MD5值去查询 </p>
     * @param fileMd5 md5
     */
    FileInfo findByFileMd5(String fileMd5);
}
