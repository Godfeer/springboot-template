package com.godfeer.rest.modular.app.login

import com.godfeer.core.bean.Phone
import com.godfeer.rest.bean.ResponseBean
import javax.servlet.http.HttpServletRequest

/**
 * @author : godfeer@aliyun.com
 * @date : 2018/6/11/011
 */

interface UserLoginService {
    /***
     * 统一登录接口
     */
    fun login(name: String, password: String, equipment: Int, request: HttpServletRequest): ResponseBean

    /**
     * 用户名登录
     */
    fun loginName(username: String, password: String, equipment: Int): ResponseBean

    /**
     * 手机号登录
     */
    fun phoneName(phone: String, password: String, equipment: Int): ResponseBean

    /**
     * 修改密码
     */
    fun changePassword(phone : Phone): ResponseBean

    /**
     * 登录终端判断
     */
    fun equipmentJudgment(equipment :Int)
}
