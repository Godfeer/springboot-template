package com.godfeer.rest.modular.app.file.controller;

import com.godfeer.core.base.controller.BaseController;
import com.godfeer.rest.modular.model.file.FileInfo;
import com.godfeer.rest.bean.dto.BaseResDto;
import com.godfeer.rest.bean.dto.ChunksReqDto;
import com.godfeer.rest.modular.model.file.FileUploadUtil;
import com.godfeer.rest.modular.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @author : godfeer@aliyun.com 
 * @date : 2018/6/22/022
 **/
@RestController
@RequestMapping(value="/file")
public class FileInfoController extends BaseController{

	private final FileRepository fileRepository;
	
	private String uploadUrl = "/upload";
	
	private String whitelist = "gif.jpg.jpeg.bmp.png.mp3.mp4.exe.txt.avi.flv.doc.docx.ppt.pptx.xls.xlsx,GIF.JPG.JPEG.BMP.PNG.MP3.MP4.EXE.TXT.AVI.FLV.DOC.DOCX."
			+ "PPT.PPTX.XLS.XLSX,zip"
			+ ",ZIP,RAR,rar";

    @Autowired
    public FileInfoController(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    /**
	 * 
	 * <p>Title: uploadFile</p>
	 * <p>Description:普通上传文件 </p>
	 * @param multipartFile
	 * @return
	 * @author JobsZhang
	 * @date 2018年6月8日 下午5:48:17
	 */
	@RequestMapping(value="/uploadFile",method=RequestMethod.POST)
	@ResponseBody
	public BaseResDto uploadFile (@RequestParam("file") MultipartFile multipartFile){
		BaseResDto brd = new BaseResDto();
		FileInfo fileInfo = null;
		try {
			fileInfo = FileUploadUtil.uploadFile(multipartFile, getHttpServletRequest(), uploadUrl, whitelist, false);
		} catch (Exception e) {
			brd.setMesg(e.getMessage());
		}
		brd.setSuccess(true);
		brd.setValues(fileInfo);
		return brd;
	}
	
	/**
	 * 
	 * <p>Title: checkFile</p>
	 * <p>Description:根据文件MD5查询文件是否存在 </p>
	 * @param fileMd5 文件唯一指纹
	 * @return
	 * @author JobsZhang
	 * @date 2018年6月8日 下午5:48:29
	 */
	@RequestMapping(value="/checkFile",method=RequestMethod.POST)
	@ResponseBody
	public BaseResDto checkFile (@RequestParam String fileMd5){
		BaseResDto brd = new BaseResDto();

		FileInfo fileInfo = fileRepository.findByFileMd5(fileMd5);
		brd.setSuccess(fileInfo!=null);
		brd.setValues(fileInfo);
		return brd;
	}
	
	/**
	 * 
	 * <p>Title: checkChunk</p>
	 * <p>Description: 检测分片是否已经存在</p>
	 * @param fileMd5 文件唯一指纹
	 * @param chunk 分片编号
	 * @param chunkSize 分片大小
	 * @return brd
	 * @author JobsZhang
	 * @date 2018年6月8日 下午5:48:56
	 */
	@RequestMapping(value="/checkChunk",method=RequestMethod.POST)
	@ResponseBody
	public BaseResDto checkChunk (@RequestParam String fileMd5, @RequestParam int chunk, @RequestParam long chunkSize){
		BaseResDto brd = new BaseResDto();
		String path = getSession().getServletContext().getRealPath(uploadUrl);
		String filePath = path+File.separator+fileMd5+File.separator+chunk;//合并文件必须要按照顺序合并，所以不能重命名		
		File file = new File(filePath);
		long length = file.length();
		if(file.exists()&&length==chunkSize){//分片已存在，这里一定要验证传过来的分片大小是否和已存在的分片大小相同，
			brd.setSuccess(true);
		}
		return brd;
	}
	
	/**
	 * 
	 * <p>Title: uploadChunks</p>
	 * <p>Description: 分片上传</p>
	 * @param fileMd5 文件唯一指纹
	 * @param chunk 分片编号
	 * @param chunkSize 分片大小
	 * @return
	 * @author JobsZhang
	 * @date 2018年6月8日 下午5:58:46
	 */
	@RequestMapping(value="/uploadChunks",method=RequestMethod.POST)
	@ResponseBody
	public BaseResDto uploadChunks (@RequestParam("file") MultipartFile multipartFile, ChunksReqDto reqDto){
		BaseResDto brd = new BaseResDto();
		FileInfo fileInfo = null;
		try {
			fileInfo = FileUploadUtil.chunksUpload(multipartFile, getHttpServletRequest(), uploadUrl, whitelist, reqDto.getFileMd5(), reqDto.getChunk());
			brd.setSuccess(true);
			brd.setValues(fileInfo);
		} catch (Exception e) {
			e.printStackTrace();
			brd.setMesg("上传出错"+e.getMessage());
		}
		return brd;
	}
	/**
	 * 上传完成合并分片
	 * @param fileMd5 文件唯一指纹
	 * @param fileName 文件名
	 * @return
	 */
	@RequestMapping(value="/mergeChunks",method=RequestMethod.POST)
	@ResponseBody
	public BaseResDto mergeChunks (@RequestParam String fileMd5, @RequestParam String fileName){
		BaseResDto brd = new BaseResDto();
		FileInfo fileInfo = null;
		try {
			fileInfo = FileUploadUtil.mergeChunks(getHttpServletRequest(), uploadUrl, whitelist, fileMd5, fileName, false);

			fileRepository.save(fileInfo);
			brd.setSuccess(true);
			brd.setValues(fileInfo);
		} catch (Exception e) {
			e.printStackTrace();
			brd.setMesg("合并文件出错"+e.getMessage());
		}
		return brd;
	}
	
}
