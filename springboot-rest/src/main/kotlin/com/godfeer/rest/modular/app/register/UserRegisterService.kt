package com.godfeer.rest.modular.app.register


import com.godfeer.rest.bean.ResponseBean
import com.godfeer.rest.modular.model.user.User
import com.godfeer.rest.modular.model.user.UserToken
import javax.servlet.http.HttpServletRequest

/**
 * @author : godfeer@aliyun.com
 * @date : 2018/6/4/004
 */
interface UserRegisterService {

    /**
     * 用户名注册查询验证
     * @param name user
     * @return any
     */
    fun nameRegisterFind(name: String, equipment: Int, ip: String): ResponseBean

    /**
     * 用户名注册
     * @param user user
     * @return any
     */
    fun nameRegister(username: String, password: String, phone: String, verification: String, request: HttpServletRequest, equipment: Int): ResponseBean

    /**
     * 手机号注册
     * @param user user
     */
    fun phoneRegister(user: User)

    /**
     * 邮箱注册
     * @param user user
     */
    fun emailRegister(user: User)

    /**
     * 短信验证码随机生成
     * @param phone
     */
    @Deprecated(": 此方法用于Math 随机生成验证码，请调用最新的验证码接口\n" +
            "      ")
    fun verificationCodeGenerate(phone: String, request: HttpServletRequest, bool: Boolean): String

    /**
     * 短信验证码验证
     * @param code 验证码值
     */
    fun verificationCodeVerification(code: String)

    /**
     * 生成 token
     * @param user user
     * @return 携带token的UserToken
     */
    fun generatingToken(user: User): UserToken
    /**
     * 注册终端判断
     */
    fun equipmentJudgment(equipment :Int)

    /**
     * 验证手机号
     */
    fun phoneRegisterFind(phone: String, equipment: Int, ip: String)
}
