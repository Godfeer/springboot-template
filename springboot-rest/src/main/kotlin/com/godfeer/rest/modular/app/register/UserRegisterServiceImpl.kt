package com.godfeer.rest.modular.app.register

import com.godfeer.core.util.RegexUtils
import com.godfeer.core.util.ToolUtil
import com.godfeer.rest.bean.ResponseBean
import com.godfeer.rest.common.enums.ErrorCode
import com.godfeer.rest.common.exception.DescribeException
import com.godfeer.rest.modular.model.user.User
import com.godfeer.rest.modular.model.user.UserToken
import com.godfeer.rest.modular.repository.UserRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import javax.servlet.http.HttpServletRequest

/**
 * @class_name : UserRegisterServiceImpl
 * @param : com.godfeer.admin.rest.modular.app.user.register
 * @describe: TODO
 * @creat_user: godfeer@aliyun.com
 * @creat_date: 2018/6/4/004
 * @creat_time: 15:30
 */
@Service
@Transactional
class UserRegisterServiceImpl : UserRegisterService {

    var log = Logger.getLogger(this::class.java)

    var map = TreeMap<String, String>()

    @Autowired
    private val ur: UserRepository? = null

    /***
     * 检验用户名是否可用
     */
    override fun nameRegisterFind(name: String, equipment: Int, ip: String): ResponseBean {
        log.info("访问IP地址为：" + ip)
        equipmentJudgment(equipment)
        var usd: User? = User()
        try {
            if (ur != null) {
                usd = ur.findByUsername(name)
                log.error("****************" + name)
            }
        } catch (e: Exception) {
            throw DescribeException(ErrorCode.ILLEGAL_ARGUMENT_EXCEPTION)
        }
        if (usd == null) {
            throw DescribeException(200, "可以注册")
        } else {
            throw DescribeException(ErrorCode.USERNAME_IS_REGISTERED_EXCEPTION)
        }
    }

    /***
     * 用户名注册方法
     */
    override fun nameRegister(username: String, password: String, phone: String, verification: String, request: HttpServletRequest, equipment: Int): ResponseBean {

        if (verification != verificationCodeGenerate(phone, request, false)) {
            throw DescribeException(ErrorCode.VERIFICATION_CODE_ERROR)
        }
        //验证用户名
        nameRegisterFind(username, equipment, RegexUtils.getIpAddr(request))
        //验证手机号
        phoneRegisterFind(phone, equipment, RegexUtils.getIpAddr(request))

        if (password.length < 8) {
            throw DescribeException(ErrorCode.PASSWORD_NOT_MEET_RULES)
        }

        if (ToolUtil.isNotEmpty(username) && ToolUtil.isNotEmpty(phone)) {
            var user: User = User()
            user.username = username

            user.password = password
            user.phone = phone
            user.createTime = ToolUtil.currentTime()

            user = ur?.save(user)!!
            return ResponseBean(200, "success", user)
        } else {
            throw DescribeException(ErrorCode.ILLEGAL_ARGUMENT_EXCEPTION)
        }
    }

    override fun phoneRegisterFind(phone: String, equipment: Int, ip: String) {

        if (RegexUtils.checkPhone(phone)) {
            val user: User? = ur?.findByPhone(phone)
            if (user != null) {
                throw DescribeException(ErrorCode.PHONE_NUMBER_EXISTS)
            }
        } else {
            throw DescribeException(ErrorCode.BAD_PHONE_NUMBER_FORMAT_EXCEPTION)
        }
    }

    /***
     * 手机号注册
     */
    override fun phoneRegister(user: User) {
        TODO()

    }

    /***
     * 邮箱注册
     */
    override fun emailRegister(user: User) {

    }

    //@Cacheable(value="verificationCodeGenerate",key = "#phone")
    override fun verificationCodeGenerate(phone: String, request: HttpServletRequest, bool: Boolean): String {

        if (!RegexUtils.checkMobile(phone)) {
            throw DescribeException(ErrorCode.BAD_PHONE_NUMBER_FORMAT_EXCEPTION)
        }
        if (bool) {
            val verfication = ToolUtil.getRandomString(6)
            map[phone] = verfication
            return verfication
        } else {

            val sss = map.get(phone)
            if (sss != null) {
                return sss
            } else {
                return ""
            }
        }
    }

    override fun verificationCodeVerification(code: String) {

    }

    override fun generatingToken(user: User): UserToken {
        return UserToken()
    }

    override fun equipmentJudgment(equipment: Int) {
        when (equipment) {
            1 -> log.info("PC")
            2 -> log.info("VR")
            3 -> log.info("Android")
            4 -> log.info("IOS")
        }
    }

}
