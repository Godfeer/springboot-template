package com.godfeer.rest.modular.model.file;

import javax.persistence.*;
import java.util.Date;
/**
 * @author : godfeer@aliyun.com 
 * @date : 2018/6/22/022
 **/

@Entity
@Table(name = "sys_file")
public class FileInfo {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    private String id;

    private String fileName;

    private Long size;

    private String type;

    private Long mediaTime;

    private Date uploadTime;

    private String path;

    private String url;

    private String fileMd5;

    public String getFileMd5() {
        return fileMd5;
    }

    public void setFileMd5(String fileMd5) {
        this.fileMd5 = fileMd5;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Long getMediaTime() {
        return mediaTime;
    }

    public void setMediaTime(Long mediaTime) {
        this.mediaTime = mediaTime;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "id='" + id + '\'' +
                ", fileName='" + fileName + '\'' +
                ", size=" + size +
                ", type='" + type + '\'' +
                ", mediaTime=" + mediaTime +
                ", uploadTime=" + uploadTime +
                ", path='" + path + '\'' +
                ", url='" + url + '\'' +
                ", fileMd5='" + fileMd5 + '\'' +
                '}';
    }
}