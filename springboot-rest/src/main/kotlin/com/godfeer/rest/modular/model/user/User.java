package com.godfeer.rest.modular.model.user;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "sys_user")
public class User {
    /**
     * tuId : 记录标识
     * toId : 所属组织
     * username : 用户名
     * password : 密码
     * phone : 手机号
     * email : 电子邮箱
     * wechat : 微信
     * qq : QQ
     * weiBo : 微博
     * nickName : 昵称
     * realName : 真实名字
     * gender : 性别
     * idCard : 身份证号
     * age : 年龄
     * constellation : 星座
     * headImgUrl : 头像地址
     * createTime : 创建时间
     * updateTime : 最新时间
     * loginCount : 登陆次数
     * loginMode : 登录方式  1 PC 2 Android 3 IOS 4 PAD
     */
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    private String tuId;
    private String toId;
    private String username;
    private String password;
    private String phone;
    private String email;
    private String wechat;
    private String qq;
    private String weiBo;
    private String nickName;
    private String realName;
    private String gender;
    private String idCard;
    private String age;
    private String constellation;
    private String headImgUrl;
    private String createTime;
    private String updateTime;
    private String loginCount;
    @NotNull
    private int loginMode;

    public String getTuId() {
        return tuId;
    }

    public void setTuId(String tuId) {
        this.tuId = tuId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWeiBo() {
        return weiBo;
    }

    public void setWeiBo(String weiBo) {
        this.weiBo = weiBo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }
    @Temporal(TemporalType.DATE) //时间精确到天
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    @Temporal(TemporalType.DATE) //时间精确到天
    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(String loginCount) {
        this.loginCount = loginCount;
    }

    public int getLoginMode() {
        return loginMode;
    }

    public void setLoginMode(int loginMode) {
        this.loginMode = loginMode;
    }

    @Override
    public String toString() {
        return "User{" +
                "tuId='" + tuId + '\'' +
                ", toId='" + toId + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", wechat='" + wechat + '\'' +
                ", qq='" + qq + '\'' +
                ", weiBo='" + weiBo + '\'' +
                ", nickName='" + nickName + '\'' +
                ", realName='" + realName + '\'' +
                ", gender='" + gender + '\'' +
                ", idCard='" + idCard + '\'' +
                ", age='" + age + '\'' +
                ", constellation='" + constellation + '\'' +
                ", headImgUrl='" + headImgUrl + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", loginCount='" + loginCount + '\'' +
                ", loginMode=" + loginMode +
                '}';
    }
}
