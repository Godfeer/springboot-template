package com.godfeer.rest.modular.app.file.controller

import org.apache.catalina.servlet4preview.http.HttpServletRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import javax.servlet.http.HttpServletResponse

/**
 * @author : godfeer@aliyun.com
 * @date : 2018/6/21/021
 **/


@RestController
class FileUploadController {

    @RequestMapping(value = ["/upload"], method = arrayOf(RequestMethod.POST))
    fun upload(request: HttpServletRequest?, response: HttpServletResponse): ModelAndView {
        val m = ModelAndView()
        val result = UploaderUtil.uploader(request, "/data", "filenameUploader", "/uploader", null, (1024 * 1024).toLong())
        m.addObject("result", result)
        return m
    }
}
