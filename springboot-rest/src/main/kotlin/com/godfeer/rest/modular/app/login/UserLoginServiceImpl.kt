package com.godfeer.rest.modular.app.login

import com.godfeer.core.bean.Phone
import com.godfeer.core.util.RegexUtils
import com.godfeer.core.util.ToolUtil
import com.godfeer.rest.bean.ResponseBean
import com.godfeer.rest.common.enums.ErrorCode
import com.godfeer.rest.common.exception.DescribeException
import com.godfeer.rest.common.utils.JWTUtil
import com.godfeer.rest.modular.model.user.UserToken
import com.godfeer.rest.modular.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import org.slf4j.LoggerFactory
import sun.security.smartcardio.SunPCSC
import javax.servlet.http.HttpServletRequest

/**
 * @author : godfeer@aliyun.com
 * @date : 2018/6/11/011
 **/
@Service
@Transactional
class UserLoginServiceImpl : UserLoginService {


    protected var log = Logger.getLogger(this.javaClass)

    var map = TreeMap<String,String>()

    @Autowired
    private val ur : UserRepository? = null


    override fun login(name: String, password: String, equipment: Int, request: HttpServletRequest): ResponseBean {

        return if (!RegexUtils.checkMobile(name)){
            loginName(name,password,equipment)
        }else{
            phoneName(name,password,equipment)
        }
    }




    override fun loginName(username: String, password: String, equipment: Int): ResponseBean {
        //判断终端
        equipmentJudgment(equipment)

        if (ToolUtil.isNotEmpty(username) && ToolUtil.isNotEmpty(password)){
            val find = ur?.getByUsernameIsAndPasswordIs(username,password)
            val u : UserToken = UserToken()
            if (ToolUtil.isNotEmpty(find)) {
                //生成token
                u.token = JWTUtil.sign(username, password)
                u.user = find
                log.error(JWTUtil.getUsername(u.token))
                return ResponseBean(200, "success", u)
            }else{
                throw DescribeException(ErrorCode.WRONG_USER_NAME_PASSWORD_EXCEPTION)
            }
        }else{
            throw DescribeException(ErrorCode.USERNAME_PASSWORD_IS_NOT_EXCEPTION)
        }
    }

    override fun phoneName(phone: String, password: String, equipment: Int): ResponseBean {
        //判断终端
        equipmentJudgment(equipment)

        if (RegexUtils.checkMobile(phone) == false){
            throw DescribeException(ErrorCode.BAD_PHONE_NUMBER_FORMAT_EXCEPTION)
        }

        if (ToolUtil.isNotEmpty(phone) && ToolUtil.isNotEmpty(password)){
            val find = ur?.getByPhoneIsAndPasswordIs(phone,password)
            val u : UserToken = UserToken()
            if (ToolUtil.isNotEmpty(find)) {
                //生成token
                u.token = JWTUtil.sign(find?.username, password)
                u.user = find
                log.error(JWTUtil.getUsername(u.token))
                return ResponseBean(200, "success", u)
            }else{
                throw DescribeException(ErrorCode.BAD_PHONE_NUMBER_IS_NOT_EXCEPTION)
            }
        }else{
            throw DescribeException(ErrorCode.BAD_PHONE_NUMBER_IS_NOT_EXCEPTION)
        }
    }

    override fun changePassword(phone: Phone): ResponseBean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun equipmentJudgment(equipment: Int) {
        when(equipment) {
            1 -> log.info("PC")
            2 -> log.info("VR")
            3 -> log.info("Android")
            4 -> log.info("IOS")
        }
    }

}