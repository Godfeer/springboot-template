package com.godfeer.rest.modular.model.file;

class UUID {
	static String getUUID(){
		return java.util.UUID.randomUUID().toString().replaceAll("-", "").trim();
	}
}
