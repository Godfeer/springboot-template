﻿package com.godfeer.rest.modular.app.login

import com.godfeer.core.base.controller.BaseController
import com.godfeer.core.util.RegexUtils
import com.godfeer.rest.bean.ResponseBean
import com.godfeer.rest.common.exception.ip.RequestLimit
import com.godfeer.rest.common.exception.ip.RequestLimitException
import com.godfeer.rest.config.properties.JwtProperties
import com.godfeer.rest.modular.repository.UserRepository
import jdk.nashorn.internal.runtime.regexp.joni.Config.log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RequestMapping("/user")
@RestController
class LoginController : BaseController()  {
    @Autowired
    private val jwtProperties: JwtProperties? = null

    @Autowired
    private val userRepository : UserRepository? = null

    @Autowired
    private val uls : UserLoginService? = null


    /**
     * 用户登录
     * @param account
     * @return
     */
    @PostMapping(value = ["/login"])
    @ResponseBody
    @RequestLimit(count=10,time=6000)
    @Throws(Exception::class)
    fun login(@RequestParam("name") name : String ,
              @RequestParam("password") password : String,
              @RequestParam("equipment") equipment :Int,
              request: HttpServletRequest): ResponseBean? {

        log.info("IP: "+ RegexUtils.getIpAddr(request))
        return  uls?.login(name,password,equipment,request)
    }

    /**
     * 用户名登录
     * @param account
     * @return
     */
    @PostMapping(value = ["/loginName"])
    @ResponseBody
    @RequestLimit(count=10,time=6000)
    @Throws(Exception::class)
    fun loginName(@RequestParam("username") username : String ,
                  @RequestParam("password") password : String,
                  @RequestParam("equipment") equipment :Int,
                  request: HttpServletRequest): ResponseBean? {

            log.info("IP: "+ RegexUtils.getIpAddr(request))
            return  uls?.loginName(username,password,equipment)
    }

    /**
     * 手机号登录
     * @param account
     * @return
     */
    @PostMapping(value = ["/loginPhone"])
    @ResponseBody
    @Throws(RequestLimitException::class)
    fun loginPhone(@RequestParam("phone") phone : String ,
                   @RequestParam("password") password : String ,
                   @RequestParam("equipment") equipment :Int,
                   request: HttpServletRequest): ResponseBean? {
        return uls?.phoneName(phone,password,equipment)
    }

}