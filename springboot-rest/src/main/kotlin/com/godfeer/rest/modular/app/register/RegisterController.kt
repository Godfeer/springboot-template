package com.godfeer.rest.modular.app.register

import com.godfeer.core.base.controller.BaseController
import com.godfeer.core.util.RegexUtils
import com.godfeer.rest.bean.ResponseBean
import com.godfeer.rest.common.exception.ip.RequestLimit
import com.godfeer.rest.common.exception.ip.RequestLimitException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest


/**
 * 用户注册的控制器
 * @author godfeer@aliyun.com
 */
@RequestMapping("/user")
@RestController
class RegisterController : BaseController() {

    @Autowired
    private val urs: UserRegisterService? = null

    /***
     * 用户名校验接口
     */
    @RequestLimit(count=10,time=6000)
    @PostMapping("/nameFind")
    @Throws(RequestLimitException::class)
    fun nameFind(@RequestParam("name") name:String,
                  @RequestParam("equipment") equipment :Int,
                 request:HttpServletRequest): ResponseBean {
        val re = urs?.nameRegisterFind(name,equipment, RegexUtils.getIpAddr(request))!!
        return re
    }

    /***
     * 用户注册接口
     */
    @RequestLimit(count=10,time=6000)
    @Throws(RequestLimitException::class)
    @PostMapping("/nameRegister")
    fun register(@RequestParam("username") username: String,
                 @RequestParam("password") password: String,
                 @RequestParam("phone") phone:String,
                 @RequestParam("verification") verification:String,
                 @RequestParam("equipment") equipment :Int,
                 request:HttpServletRequest ): ResponseBean? {

        return  urs?.nameRegister(username,password,phone,verification, request,equipment)

    }


    /***
     * 手机号注册接口
     */
    @RequestLimit(count=10,time=6000)
    @Throws(RequestLimitException::class)
    @PostMapping("/phoneRegister")
    fun phoneRegister(@RequestParam("password") password: String,
                 @RequestParam("phone") phone:String,
                 @RequestParam("verification") verification:String,
                 @RequestParam("equipment") equipment :Int,
                 request:HttpServletRequest ): ResponseBean? {

        return  urs?.nameRegister("VIP_"+RegexUtils.getRandomJianHan(5),password,phone,verification, request,equipment)

    }
    /***
     * 验证码接口
     */
    @PostMapping("/verification")
    @RequestLimit(count=5,time=60000)
    @Throws(Exception::class)
    fun verificationCodeGenerate(@RequestParam("phone") phone:String,
                                 request:HttpServletRequest ): ResponseBean? {

        return ResponseBean(200,"success",urs?.verificationCodeGenerate(phone,request,true))
    }


}