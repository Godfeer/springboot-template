package com.godfeer.rest.common.aop;

import com.godfeer.core.exception.BaseException;
import com.godfeer.rest.common.exception.DescribeException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * controller 增强器
 * @author : godfeer@aliyun.com
 * @date : 2018/6/11/011
 **/
@ControllerAdvice
public class ExceptionAdvice{

    /**
     * 全局异常捕捉处理
     * @param ex e
     * @return map
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Map errorHandler(Exception ex) {
        Map map = new HashMap();
        map.put("code", 500);
        map.put("msg", ex.getMessage());
        return map;
    }

    /**
     * 拦截捕捉自定义异常 MyException.class
     * @param ex ex
     * @return map
     */
    @ResponseBody
    @ExceptionHandler(value = BaseException.class)
    public Map myErrorHandler(DescribeException ex) {
        Map map = new HashMap();
        map.put("code", ex.getCode() );
        map.put("msg", ex.getMessage());
        return map;
    }

}