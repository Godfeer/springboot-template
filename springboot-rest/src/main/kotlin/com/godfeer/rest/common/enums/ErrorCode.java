package com.godfeer.rest.common.enums;

import lombok.Getter;

/**
 * Error code enum<br><br>
 * <b>0.0.4: </b>Add org.springframework.http.converter.HttpMessageNotReadableException
 *
 * @author lshaci
 * @since 0.0.3
 * @version 0.0.4
 */
@Getter
public enum ErrorCode {

    /**
     * com.xdbigdata.framework.web.exception.NotLoginException
     */
    NOT_LOGIN_EXCEPTION(40001, "登录失效，请重新登录", "com.xdbigdata.framework.web.exception.NotLoginException"),
    /**
     * com.xdbigdata.framework.common.exception.BaseException
     */
    INTERNAL_PROGRAM_ERROR(50000, "程序内部错误，操作失败", "com.xdbigdata.framework.common.exception.BaseException"),
    /**
     * org.springframework.dao.DataAccessException
     */
    DATA_ACCESS_EXCEPTION(50001, "数据库操作失败", "org.springframework.dao.DataAccessException"),
    /**
     * com.mysql.jdbc.CommunicationsException
     */
    COMMUNICATIONS_EXCEPTION(50002, "数据库连接中断", "com.mysql.jdbc.CommunicationsException"),
    /**
     * org.springframework.dao.DataIntegrityViolationException
     */
    DATA_INTEGRITY_VIOLATION_EXCEPTION(50003, "数据异常, 操作失败", "org.springframework.dao.DataIntegrityViolationException"),
    /**
     * com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException
     */
    MYSQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION(50004, "数据异常, 操作失败", "com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException"),
    /**
     * java.lang.NullPointerException
     */
    NULL_POINTER_EXCEPTION(50005, "对象未经初始化或不存在", "java.lang.NullPointerException"),
    /**
     * java.io.IOException
     */
    IO_EXCEPTION(50006, "IO异常", "java.io.IOException"),
    /**
     * java.lang.ClassNotFoundException
     */
    CLASS_NOT_FOUND_EXCEPTION(50007, "指定的类不存在", "java.lang.ClassNotFoundException"),
    /**
     * java.lang.ArithmeticException
     */
    ARITHMETIC_EXCEPTION(50008, "数学运算异常", "java.lang.ArithmeticException"),
    /**
     * java.lang.ArrayIndexOutOfBoundsException
     */
    ARRAY_INDEX_OUT_OF_BOUNDS_EXCEPTION(50009, "数组下标越界", "java.lang.ArrayIndexOutOfBoundsException"),
    /**
     * java.lang.IllegalArgumentException
     */
    ILLEGAL_ARGUMENT_EXCEPTION(50010, "参数错误或非法", "java.lang.IllegalArgumentException"),
    /**
     * java.lang.ClassCastException
     */
    CLASS_CAST_EXCEPTION(50011, "类型强制转换错误", "java.lang.ClassCastException"),
    /**
     * java.sql.SQLException
     */
    SQL_EXCEPTION(50013, "操作数据库异常", "java.sql.SQLException"),
    /**
     * java.lang.SecurityException
     */
    SECURITY_EXCEPTION(50012, "违背安全原则异常", "java.lang.SecurityException"),
    /**
     * java.lang.NoSuchMethodException
     */
    NOSUCH_METHOD_EXCEPTION(50014, "方法未找到异常", "java.lang.NoSuchMethodException"),
    /**
     * java.net.ConnectException
     */
    CONNECT_EXCEPTION(50016, "服务器连接异常", "java.net.ConnectException"),
    /**
     * java.util.concurrent.CancellationException
     */
    CANCELLATION_EXCEPTION(50017, "任务已被取消的异常", "java.util.concurrent.CancellationException"),
    /**
     * java.util.concurrent.CancellationException
     */
    PARSE_EXCEPTION(50019, "日期格式错误", "java.text.ParseException"),
    /**
     * com.mysql.jdbc.MysqlDataTruncation
     */
    MYSQL_DATA_TRUNCATION_EXCEPTION(50020, "服务器不能接收所有数据", "com.mysql.jdbc.MysqlDataTruncation"),
    /**
     * org.springframework.http.converter.HttpMessageNotReadableException
     */
    HTTP_MESSAGE_NOT_READABLE_EXCEPTION(50021, "参数转换异常", "org.springframework.http.converter.HttpMessageNotReadableException"),
    /**
     * com.godfeer.rest.modular.app.login.loginName
     */
    WRONG_USER_NAME_PASSWORD_EXCEPTION(50022, "用户名或密码错误", "com.godfeer.rest.modular.app.login.loginName"),
    USERNAME_PASSWORD_IS_NOT_EXCEPTION(50023,"填写的用户名或密码有误","com.godfeer.rest.modular.app.login.loginName"),
    BAD_PHONE_NUMBER_FORMAT_EXCEPTION(50024,"手机号格式有误，请重新输入","com.godfeer.rest.modular.app.login.loginName"),
    BAD_PHONE_NUMBER_IS_NOT_EXCEPTION(50025,"手机号或密码错误","com.godfeer.rest.modular.app.login.loginName"),
    USERNAME_IS_REGISTERED_EXCEPTION(50026,"该用户名已注册","com.godfeer.rest.modular.app.login.loginName"),
    VERIFICATION_CODE_ERROR(50026,"验证码错误","com.godfeer.rest.modular.app.login.loginName"),
    PASSWORD_NOT_MEET_RULES(50027,"密码不符合规则,请至少大于八位数且包含英文大小写","com.godfeer.rest.modular.app.login.loginName"),
    PHONE_NUMBER_EXISTS(50028,"手机号已存在","com.godfeer.rest.modular.app.login.loginName"),

    ;



    private int code;
    private String msg;
    private String exceptionClass;

    private ErrorCode(int code, String msg, String exceptionClass) {
        this.code = code;
        this.msg = msg;
        this.exceptionClass = exceptionClass;
    }

    /**
     * Get error code by exception
     *
     * @param exception the exception
     * @return the error code
     */
    public static ErrorCode getByException(Exception exception) {
        if (exception != null) {
            ErrorCode[] errorCodes = ErrorCode.values();
            for (ErrorCode errorCode : errorCodes) {
                if (exception.getClass().getName().equals(errorCode.exceptionClass)) {
                    return errorCode;
                }
            }
        }
        return INTERNAL_PROGRAM_ERROR;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getExceptionClass() {
        return exceptionClass;
    }

    public void setExceptionClass(String exceptionClass) {
        this.exceptionClass = exceptionClass;
    }
}