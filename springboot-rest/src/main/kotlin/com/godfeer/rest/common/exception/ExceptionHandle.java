package com.godfeer.rest.common.exception;

import com.godfeer.rest.bean.ResponseBean;
import com.godfeer.rest.common.enums.ErrorCode;
import com.godfeer.rest.common.enums.ExceptionEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandle {

    private final static Logger LOGGER = Logger.getLogger(ExceptionHandle.class);

    /**
     * 判断错误是否是已定义的已知错误，不是则由未知错误代替，同时记录在log中
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseBean exceptionGet(Exception e){
        if(e instanceof DescribeException){
            DescribeException myException = (DescribeException) e;
            LOGGER.error(myException.getMessage());
            throw new DescribeException(myException.getCode(),myException.getMessage());
        }else {
            throw new DescribeException(ErrorCode.ILLEGAL_ARGUMENT_EXCEPTION);
        }
    }
}
