package com.godfeer.rest.common.exception;

import com.godfeer.rest.common.enums.ErrorCode;
import com.godfeer.rest.common.enums.ExceptionEnum;

/**
 * @author : godfeer@aliyun.com
 * @date : 2018/6/11/011
 **/
public class DescribeException extends RuntimeException{

    private Integer code;
    private String exceptionClass;

    /**
     * 继承exception，加入错误状态值
     * @param errorCode errorCode
     */
    public DescribeException(ErrorCode errorCode) {
        super(errorCode.getMsg());
        this.code = errorCode.getCode();
        this.exceptionClass = errorCode.getExceptionClass();
    }

    /**
     * 自定义错误信息
     * @param message msg
     * @param code code
     */
    public DescribeException( Integer code ,String message) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getExceptionClass() {
        return exceptionClass;
    }

    public void setExceptionClass(String exceptionClass) {
        this.exceptionClass = exceptionClass;
    }
}
