package com.godfeer.rest.common.exception.ip;

public class RequestLimitException extends Exception {
    private static final long serialVersionUID = 1364225358754654702L;

    public RequestLimitException(){
        super("HTTP请求超出设定的限制,请过段时间访问");
    }
    public RequestLimitException(String message){
        super(message);
    }

}