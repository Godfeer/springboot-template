/**
 * 初始化文件上传详情对话框
 */
var UploadInfoDlg = {
    uploadInfoData : {}
};

/**
 * 清除数据
 */
UploadInfoDlg.clearData = function() {
    this.uploadInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UploadInfoDlg.set = function(key, val) {
    this.uploadInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UploadInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UploadInfoDlg.close = function() {
    parent.layer.close(window.parent.Upload.layerIndex);
}

/**
 * 收集数据
 */
UploadInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('type')
    .set('address')
    .set('createtime')
    .set('secret')
    .set('length');
}
UploadInfoDlg.issubmit = function (data){
    this.clearData();
    this.collectData();

        Feng.success("添加成功!");
        window.parent.Upload.table.refresh();
        UploadInfoDlg.close();


}

/**
 * 提交添加
 */
UploadInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/upload/add", function(data){

        Feng.success("添加成功!");
        window.parent.Upload.table.refresh();
        UploadInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.uploadInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UploadInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/upload/update", function(data){
        Feng.success("修改成功!");
        window.parent.Upload.table.refresh();
        UploadInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.uploadInfoData);
    ajax.start();
}

$(function() {

});
