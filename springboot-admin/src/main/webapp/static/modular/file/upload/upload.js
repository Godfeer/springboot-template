/**
 * 文件上传管理初始化
 */
var Upload = {
    id: "UploadTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Upload.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '文件名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '文本类型', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '文件路径', field: 'address', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '是否加密(默认为0不加密,1加密)', field: 'secret', visible: true, align: 'center', valign: 'middle'},
            {title: '文件大小(M)', field: 'length', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Upload.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Upload.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加文件上传
 */
Upload.openAddUpload = function () {
    var index = layer.open({
        type: 2,
        title: '添加文件上传',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/upload/upload_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看文件上传详情
 */
Upload.openUploadDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '文件上传详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/upload/upload_update/' + Upload.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除文件上传
 */
Upload.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/upload/delete", function (data) {
            Feng.success("删除成功!");
            Upload.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("uploadId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询文件上传列表
 */
Upload.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Upload.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Upload.initColumn();
    var table = new BSTable(Upload.id, "/upload/list", defaultColunms);
    table.setPaginationType("client");
    Upload.table = table.init();
});
