/**
 * 视频上传管理初始化
 */
var Video = {
    id: "VideoTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Video.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '视频名', field: 'title', visible: true, align: 'center', valign: 'middle'},
            {title: '视频简介', field: 'videoIntroduce', visible: true, align: 'center', valign: 'middle'},
            {title: '视频类型', field: 'videoType', visible: true, align: 'center', valign: 'middle'},
            {title: '视频类别', field: 'videoClass', visible: true, align: 'center', valign: 'middle'},
            {title: '视频下载路径', field: 'videoUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '图片名称', field: 'imgTitle', visible: true, align: 'center', valign: 'middle'},
            {title: '图片地址', field: 'imgUrl', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Video.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Video.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加视频上传
 */
Video.openAddVideo = function () {
    var index = layer.open({
        type: 2,
        title: '添加视频上传',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/video/video_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看视频上传详情
 */
Video.openVideoDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '视频上传详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/video/video_update/' + Video.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除视频上传
 */
Video.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/video/delete", function (data) {
            Feng.success("删除成功!");
            Video.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("videoId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询视频上传列表
 */
Video.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Video.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Video.initColumn();
    var table = new BSTable(Video.id, "/video/list", defaultColunms);
    table.setPaginationType("client");
    Video.table = table.init();
});
