/**
 * 用户列表管理初始化
 */
var Userlogin = {
    id: "UserloginTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Userlogin.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '用户id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '用户名', field: 'username', visible: true, align: 'center', valign: 'middle'},
            {title: '用户密码', field: 'password', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
            {title: '微信号', field: 'wechat', visible: true, align: 'center', valign: 'middle'},
            {title: 'QQ号', field: 'qq', visible: true, align: 'center', valign: 'middle'},
            {title: '微博账号', field: 'weibo', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
            {title: '真实姓名', field: 'realname', visible: true, align: 'center', valign: 'middle'},
            {title: '性别 :0、男 1、女', field: 'gender', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证号', field: 'idcard', visible: true, align: 'center', valign: 'middle'},
            {title: '出生日期', field: 'birthday', visible: true, align: 'center', valign: 'middle'},
            {title: '年龄', field: 'age', visible: true, align: 'center', valign: 'middle'},
            {title: '头像', field: 'headimgurl', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '更新时间', field: 'updatetime', visible: true, align: 'center', valign: 'middle'},
            {title: '删除标识', field: 'deleteflag', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Userlogin.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Userlogin.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户列表
 */
Userlogin.openAddUserlogin = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户列表',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/userlogin/userlogin_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户列表详情
 */
Userlogin.openUserloginDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户列表详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/userlogin/userlogin_update/' + Userlogin.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户列表
 */
Userlogin.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/userlogin/delete", function (data) {
            Feng.success("删除成功!");
            Userlogin.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("userloginId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户列表列表
 */
Userlogin.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Userlogin.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Userlogin.initColumn();
    var table = new BSTable(Userlogin.id, "/userlogin/list", defaultColunms);
    table.setPaginationType("client");
    Userlogin.table = table.init();
});
