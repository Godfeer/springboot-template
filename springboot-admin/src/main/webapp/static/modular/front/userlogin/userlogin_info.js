/**
 * 初始化用户列表详情对话框
 */
var UserloginInfoDlg = {
    userloginInfoData : {}
};

/**
 * 清除数据
 */
UserloginInfoDlg.clearData = function() {
    this.userloginInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UserloginInfoDlg.set = function(key, val) {
    this.userloginInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UserloginInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UserloginInfoDlg.close = function() {
    parent.layer.close(window.parent.Userlogin.layerIndex);
}

/**
 * 收集数据
 */
UserloginInfoDlg.collectData = function() {
    this
    .set('id')
    .set('username')
    .set('password')
    .set('phone')
    .set('email')
    .set('wechat')
    .set('qq')
    .set('weibo')
    .set('nickname')
    .set('realname')
    .set('gender')
    .set('idcard')
    .set('birthday')
    .set('age')
    .set('headimgurl')
    .set('createtime')
    .set('updatetime')
    .set('deleteflag');
}

/**
 * 提交添加
 */
UserloginInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/userlogin/add", function(data){
        Feng.success("添加成功!");
        window.parent.Userlogin.table.refresh();
        UserloginInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.userloginInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UserloginInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/userlogin/update", function(data){
        Feng.success("修改成功!");
        window.parent.Userlogin.table.refresh();
        UserloginInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.userloginInfoData);
    ajax.start();
}

$(function() {

});
