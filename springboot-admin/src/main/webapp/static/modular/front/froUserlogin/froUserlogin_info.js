/**
 * 初始化用户列表详情对话框
 */
var FroUserloginInfoDlg = {
    froUserloginInfoData : {}
};

/**
 * 清除数据
 */
FroUserloginInfoDlg.clearData = function() {
    this.froUserloginInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
FroUserloginInfoDlg.set = function(key, val) {
    this.froUserloginInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
FroUserloginInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
FroUserloginInfoDlg.close = function() {
    parent.layer.close(window.parent.FroUserlogin.layerIndex);
}

/**
 * 收集数据
 */
FroUserloginInfoDlg.collectData = function() {
    this
    .set('id')
    .set('username')
    .set('password')
    .set('phone')
    .set('email')
    .set('wechat')
    .set('qq')
    .set('weibo')
    .set('nickname')
    .set('realname')
    .set('gender')
    .set('idcard')
    .set('birthday')
    .set('age')
    .set('headimgurl')
    .set('createtime')
    .set('updatetime')
    .set('deleteflag');
}

/**
 * 提交添加
 */
FroUserloginInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/froUserlogin/add", function(data){
        Feng.success("添加成功!");
        window.parent.FroUserlogin.table.refresh();
        FroUserloginInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.froUserloginInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
FroUserloginInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/froUserlogin/update", function(data){
        Feng.success("修改成功!");
        window.parent.FroUserlogin.table.refresh();
        FroUserloginInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.froUserloginInfoData);
    ajax.start();
}

$(function() {

});
