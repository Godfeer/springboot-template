/**
 * 用户列表管理初始化
 */
var FroUserlogin = {
    id: "FroUserloginTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
FroUserlogin.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '用户名', field: 'username', visible: true, align: 'center', valign: 'middle'},
            {title: '用户密码', field: 'password', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            {title: '真实姓名', field: 'realname', visible: true, align: 'center', valign: 'middle'},
            {title: '性别', field: 'gender', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证号', field: 'idcard', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
FroUserlogin.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        FroUserlogin.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户列表
 */
FroUserlogin.openAddFroUserlogin = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户列表',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/froUserlogin/froUserlogin_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户列表详情
 */
FroUserlogin.openFroUserloginDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户列表详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/froUserlogin/froUserlogin_update/' + FroUserlogin.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户列表
 */
FroUserlogin.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/froUserlogin/delete", function (data) {
            Feng.success("删除成功!");
            FroUserlogin.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("froUserloginId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户列表列表
 */
FroUserlogin.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    FroUserlogin.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = FroUserlogin.initColumn();
    var table = new BSTable(FroUserlogin.id, "/froUserlogin/list", defaultColunms);
    table.setPaginationType("client");
    FroUserlogin.table = table.init();
});
