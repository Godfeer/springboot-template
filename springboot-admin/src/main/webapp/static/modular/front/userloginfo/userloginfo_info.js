/**
 * 初始化前端用户详情详情对话框
 */
var UserloginfoInfoDlg = {
    userloginfoInfoData : {}
};

/**
 * 清除数据
 */
UserloginfoInfoDlg.clearData = function() {
    this.userloginfoInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UserloginfoInfoDlg.set = function(key, val) {
    this.userloginfoInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UserloginfoInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UserloginfoInfoDlg.close = function() {
    parent.layer.close(window.parent.Userloginfo.layerIndex);
}

/**
 * 收集数据
 */
UserloginfoInfoDlg.collectData = function() {
    this
    .set('userid')
    .set('credit')
    .set('musictag')
    .set('startag')
    .set('mytag')
    .set('companyname')
    .set('industryid')
    .set('position')
    .set('joincount')
    .set('misscount')
    .set('cautionmoneyflag')
    .set('receivemsg')
    .set('soundremind')
    .set('shockremind')
    .set('lotterynum')
    .set('ext1')
    .set('ext2')
    .set('ext3');
}

/**
 * 提交添加
 */
UserloginfoInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/userloginfo/add", function(data){
        Feng.success("添加成功!");
        window.parent.Userloginfo.table.refresh();
        UserloginfoInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.userloginfoInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UserloginfoInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/userloginfo/update", function(data){
        Feng.success("修改成功!");
        window.parent.Userloginfo.table.refresh();
        UserloginfoInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.userloginfoInfoData);
    ajax.start();
}

$(function() {

});
