/**
 * 前端用户详情管理初始化
 */
var Userloginfo = {
    id: "UserloginfoTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Userloginfo.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '用户id', field: 'userid', visible: true, align: 'center', valign: 'middle'},
            {title: '信用值', field: 'credit', visible: true, align: 'center', valign: 'middle'},
            {title: '音乐标签', field: 'musictag', visible: true, align: 'center', valign: 'middle'},
            {title: '明星标签', field: 'startag', visible: true, align: 'center', valign: 'middle'},
            {title: '我的自定义标签', field: 'mytag', visible: true, align: 'center', valign: 'middle'},
            {title: '公司名称', field: 'companyname', visible: true, align: 'center', valign: 'middle'},
            {title: '行业id', field: 'industryid', visible: true, align: 'center', valign: 'middle'},
            {title: '职业', field: 'position', visible: true, align: 'center', valign: 'middle'},
            {title: '累计参加次数', field: 'joincount', visible: true, align: 'center', valign: 'middle'},
            {title: '累计爽约次数', field: 'misscount', visible: true, align: 'center', valign: 'middle'},
            {title: '是否缴纳保证金标识', field: 'cautionmoneyflag', visible: true, align: 'center', valign: 'middle'},
            {title: '接受消息标识', field: 'receivemsg', visible: true, align: 'center', valign: 'middle'},
            {title: '声音提示标识', field: 'soundremind', visible: true, align: 'center', valign: 'middle'},
            {title: '震动提示标识', field: 'shockremind', visible: true, align: 'center', valign: 'middle'},
            {title: '抽奖次数', field: 'lotterynum', visible: true, align: 'center', valign: 'middle'},
            {title: '扩展字段', field: 'ext1', visible: true, align: 'center', valign: 'middle'},
            {title: '扩展字段', field: 'ext2', visible: true, align: 'center', valign: 'middle'},
            {title: '扩展字段', field: 'ext3', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Userloginfo.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Userloginfo.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加前端用户详情
 */
Userloginfo.openAddUserloginfo = function () {
    var index = layer.open({
        type: 2,
        title: '添加前端用户详情',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/userloginfo/userloginfo_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看前端用户详情详情
 */
Userloginfo.openUserloginfoDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '前端用户详情详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/userloginfo/userloginfo_update/' + Userloginfo.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除前端用户详情
 */
Userloginfo.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/userloginfo/delete", function (data) {
            Feng.success("删除成功!");
            Userloginfo.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("userloginfoId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询前端用户详情列表
 */
Userloginfo.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Userloginfo.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Userloginfo.initColumn();
    var table = new BSTable(Userloginfo.id, "/userloginfo/list", defaultColunms);
    table.setPaginationType("client");
    Userloginfo.table = table.init();
});
