package com.godfeer.admin.core.common.constant.dictmap.base;

/**
 * 系统相关的字典
 *
 * @author fengshuonan
 * @date 2017-05-06 15:48
 */
public class SystemDict extends com.godfeer.core.common.constant.dictmap.base.AbstractDictMap {

    @Override
    public void init() {

    }

    @Override
    protected void initBeWrapped() {

    }
}
