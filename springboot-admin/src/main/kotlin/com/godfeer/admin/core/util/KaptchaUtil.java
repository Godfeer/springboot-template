package com.godfeer.core.util;

import com.godfeer.admin.config.properties.WebProperties;

/**
 * 验证码工具类
 */
public class KaptchaUtil {

    /**
     * 获取验证码开关
     */
    public static Boolean getKaptchaOnOff() {
        return SpringContextHolder.getBean(WebProperties.class).getKaptchaOpen();
    }
}