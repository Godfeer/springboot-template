package com.godfeer.admin.myexception;

/**
 * COS自定义异常
 */
public class COSException extends RuntimeException{

    public COSException(Throwable e){
        super(e);
    }
    public COSException(String message){
        super(message);
    }

}
