package com.godfeer.admin.myconfig.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = RestTemplateProperties.PREFIX)
public class RestTemplateProperties {

    public static final String PREFIX = "restTemplate";//设置前缀

    private String url;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {


        return "RestTemplateProperties{" +
                "url='" + url + '\'' +
                '}';
    }
}
