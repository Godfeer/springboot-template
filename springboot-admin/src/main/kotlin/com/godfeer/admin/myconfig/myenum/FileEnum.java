package com.godfeer.admin.myconfig.myenum;

/**
 * 文件上传枚举
 */
public enum FileEnum {

    EMPTY("没有选择文件，请选择文件！"),
    SUCCESS("COS文件操作成功！"),
    DEFAULT("COS文件操作失败！");

    String message;

    FileEnum(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "FileEnum{" +
                "message='" + message + '\'' +
                '}';
    }
}
