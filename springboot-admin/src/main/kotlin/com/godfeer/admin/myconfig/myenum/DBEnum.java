package com.godfeer.admin.myconfig.myenum;

/**
 * 数据库操作枚举
 */
public enum DBEnum {

    SUCCESS("数据库操作成功！"),
    DEFAULT("数据库操作失败！");

    String message;

    DBEnum(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
