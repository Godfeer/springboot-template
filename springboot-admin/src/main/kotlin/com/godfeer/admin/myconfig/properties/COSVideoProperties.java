package com.godfeer.admin.myconfig.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * COS文件上传属性配置
 */

@Configuration
@ConfigurationProperties(prefix = COSVideoProperties.PREFIX)
public class COSVideoProperties {

    public static final String PREFIX = "cos.video";//设置前缀

    private String bucketName;//BUCKET

    private String bucketAppid;//APPID

    private String bucket;

    private String videoPath = "/video/";

    private String imagePath = "/image/";


    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketAppid() {
        return bucketAppid;
    }

    public void setBucketAppid(String bucketAppid) {
        this.bucketAppid = bucketAppid;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public String toString() {
        return "COSVideoProperties{" +
                "bucketName='" + bucketName + '\'' +
                ", bucketAppid='" + bucketAppid + '\'' +
                ", bucket='" + bucket + '\'' +
                ", videoPath='" + videoPath + '\'' +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }
}
