package com.godfeer.admin.myconfig;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.region.Region;
import com.godfeer.core.my.myconfig.properties.COSClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class COSConfig {


    @Autowired
    private COSClientProperties cosClientProperties;

    @Bean
    public COSClient getCOSClient(){
        String ACCESS_KEY = cosClientProperties.getAccessKey();//secretId
        String SECRET_KEY = cosClientProperties.getSecretKey();//secretKey
        String REGION_NAME = cosClientProperties.getRegion_name();//region_name

        // 1 初始化用户身份信息(secretId, secretKey)
        COSCredentials cred = new BasicCOSCredentials(ACCESS_KEY, SECRET_KEY);
        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        ClientConfig clientConfig = new ClientConfig(new Region(REGION_NAME));
        // 3 生成cos客户端
        COSClient cosClient = new COSClient(cred, clientConfig);
        return cosClient;
    }
}
