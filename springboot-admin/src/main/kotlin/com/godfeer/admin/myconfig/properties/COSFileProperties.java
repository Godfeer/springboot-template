package com.godfeer.admin.myconfig.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * COS文件上传属性配置
 */

@Configuration
@ConfigurationProperties(prefix = COSFileProperties.PREFIX)
public class COSFileProperties {

    public static final String PREFIX = "cos.file";//设置前缀

    private String bucketName;//BUCKET

    private String bucketAppid;//APPID

    private String bucket;

    private String path = "/";


    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketAppid() {
        return bucketAppid;
    }

    public void setBucketAppid(String bucketAppid) {
        this.bucketAppid = bucketAppid;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "COSFileProperties{" +
                "bucketName='" + bucketName + '\'' +
                ", bucketAppid='" + bucketAppid + '\'' +
                ", bucket='" + bucket + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
