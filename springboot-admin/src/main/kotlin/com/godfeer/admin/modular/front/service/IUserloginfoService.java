package com.godfeer.admin.modular.front.service;

import com.godfeer.admin.modular.system.model.Userlogin;
import com.godfeer.admin.modular.system.model.Userloginfo;
import com.baomidou.mybatisplus.service.IService;


/**
 * <p>
 * 用户信息详情表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-03-22
 */
public interface IUserloginfoService extends IService<Userloginfo> {



}
