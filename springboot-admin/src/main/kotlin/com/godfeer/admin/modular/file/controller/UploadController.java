package com.godfeer.admin.modular.file.controller;

import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import com.godfeer.core.base.controller.BaseController;
import com.godfeer.core.base.tips.SuccessTip;
import com.godfeer.core.log.LogObjectHolder;
import com.godfeer.admin.modular.file.service.IUploadService;
import com.godfeer.admin.modular.system.model.Upload;
import com.godfeer.admin.myconfig.myenum.DBEnum;
import com.godfeer.admin.myconfig.myenum.FileEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 文件上传控制器
 *
 * @author fengshuonan
 * @Date 2018-04-04 11:52:39
 */
@Controller
@RequestMapping("/upload")
public class UploadController extends BaseController {

    private String PREFIX = "/file/upload/";

    @Autowired
    private IUploadService uploadService;

    @Autowired
    private FileController fileController;

    /**
     * 跳转到文件上传首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "upload.html";
    }

    /**
     * 跳转到添加文件上传
     */
    @RequestMapping("/upload_add")
    public String uploadAdd() {
        return PREFIX + "upload_add.html";
    }

    /**
     * 跳转到修改文件上传
     */
    @RequestMapping("/upload_update/{uploadId}")
    public String uploadUpdate(@PathVariable Integer uploadId, Model model) {
        Upload upload = uploadService.selectById(uploadId);
        model.addAttribute("item",upload);
        LogObjectHolder.me().set(upload);
        return PREFIX + "upload_edit.html";
    }

    /**
     * 获取文件上传列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return uploadService.selectList(null);
    }


    /**
     * 新增文件上传
     */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public Object add(Upload upload) {
        uploadService.insert(upload);
        return SUCCESS_TIP;
    }

    /**
     * 删除文件上传
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer uploadId) {
        //根据id获取文件的详细信息
        Upload upload = uploadService.selectById(uploadId);
        //COS删除文件
        try{
            fileController.deleteFile(upload);
        }catch(CosServiceException e){
            return getErrorTip(FileEnum.DEFAULT.getMessage(),e.toString());
        }catch(CosClientException e){
            return getErrorTip(FileEnum.DEFAULT.getMessage(),e.toString());
        }
        //删除数据库记录
        try{
            uploadService.deleteById(uploadId);
        }catch(Exception e){
            return getErrorTip(DBEnum.DEFAULT.getMessage(),upload);
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改文件上传
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Upload upload) {
        uploadService.updateById(upload);
        return SUCCESS_TIP;
    }

    /**
     * 文件上传详情
     */
    @RequestMapping(value = "/detail/{uploadId}")
    @ResponseBody
    public Object detail(@PathVariable("uploadId") Integer uploadId) {
        return uploadService.selectById(uploadId);
    }
}
