package com.godfeer.admin.modular.front.controller;

import com.godfeer.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.godfeer.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.godfeer.admin.modular.system.model.Userloginfo;
import com.godfeer.admin.modular.front.service.IUserloginfoService;

/**
 * 前端用户详情控制器
 *
 * @author fengshuonan
 * @Date 2018-03-22 14:16:49
 */
@Controller
@RequestMapping("/userloginfo")
public class UserloginfoController extends BaseController {

    private String PREFIX = "/front/userloginfo/";

    @Autowired
    private IUserloginfoService userloginfoService;

    /**
     * 跳转到前端用户详情首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "userloginfo.html";
    }

    /**
     * 跳转到添加前端用户详情
     */
    @RequestMapping("/userloginfo_add")
    public String userloginfoAdd() {
        return PREFIX + "userloginfo_add.html";
    }

    /**
     * 跳转到修改前端用户详情
     */
    @RequestMapping("/userloginfo_update/{userloginfoId}")
    public String userloginfoUpdate(@PathVariable Integer userloginfoId, Model model) {
        Userloginfo userloginfo = userloginfoService.selectById(userloginfoId);
        model.addAttribute("item",userloginfo);
        LogObjectHolder.me().set(userloginfo);
        return PREFIX + "userloginfo_edit.html";
    }

    /**
     * 获取前端用户详情列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return userloginfoService.selectList(null);
    }

    /**
     * 新增前端用户详情
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Userloginfo userloginfo) {
        userloginfoService.insert(userloginfo);
        return SUCCESS_TIP;
    }

    /**
     * 删除前端用户详情
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer userloginfoId) {
        userloginfoService.deleteById(userloginfoId);
        return SUCCESS_TIP;
    }

    /**
     * 修改前端用户详情
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Userloginfo userloginfo) {
        userloginfoService.updateById(userloginfo);
        return SUCCESS_TIP;
    }

    /**
     * 前端用户详情详情
     */
    @RequestMapping(value = "/detail/{userloginfoId}")
    @ResponseBody
    public Object detail(@PathVariable("userloginfoId") Integer userloginfoId) {
        return userloginfoService.selectById(userloginfoId);
    }
}
