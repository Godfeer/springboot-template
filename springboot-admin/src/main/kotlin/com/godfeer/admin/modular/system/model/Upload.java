package com.godfeer.admin.modular.system.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author cz123
 * @since 2018-04-04
 */
@TableName("file_upload")
public class Upload extends Model<Upload> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 文件名称
     */
    private String name;
    /**
     * 文本类型
     */
    private String type;
    /**
     * 文件路径
     */
    private String address;
    /**
     * 创建时间
     */
    private String createtime;
    /**
     * 是否加密(默认为0不加密,1加密)
     */
    private String secret;
    /**
     * 文件大小(M)
     */
    private Long length;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Upload{" +
        "id=" + id +
        ", name=" + name +
        ", type=" + type +
        ", address=" + address +
        ", createtime=" + createtime +
        ", secret=" + secret +
        ", length=" + length +
        "}";
    }
}
