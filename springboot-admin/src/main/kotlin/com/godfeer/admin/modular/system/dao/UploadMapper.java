package com.godfeer.admin.modular.system.dao;

import com.godfeer.admin.modular.system.model.Upload;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cz123
 * @since 2018-04-04
 */
public interface UploadMapper extends BaseMapper<Upload> {


    void deleteFileByName(Upload upload);

}
