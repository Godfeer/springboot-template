package com.godfeer.admin.modular.front.service.impl;

import com.godfeer.admin.modular.system.model.Userlogin;
import com.godfeer.admin.modular.system.dao.UserloginMapper;
import com.godfeer.admin.modular.front.service.IUserloginService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author cz123
 * @since 2018-04-01
 */
@Service
public class UserloginServiceImpl extends ServiceImpl<UserloginMapper, Userlogin> implements IUserloginService {
    @Resource
    private UserloginMapper userloginMapper;

    @Override
    public Userlogin selectByName(String username) {
        return baseMapper.selectByName(username);
    }

    @Override
    public boolean verRegOrNot(Map<String,String> varMap){
        //用户名是否存在
        Userlogin userlogin = baseMapper.selectByName(varMap.get("username"));
        //手机号是否存在
        Userlogin user = userloginMapper.verRegOrNot(varMap);
        //用户不存在返回false
        if(user == null && userlogin == null){
            return false;
        }
        return true;
    }
}
