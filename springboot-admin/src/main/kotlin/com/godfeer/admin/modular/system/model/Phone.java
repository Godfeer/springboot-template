package com.godfeer.admin.modular.system.model;


import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Objects;

/**
 * 手机验证码实体类
 */

public class Phone implements Serializable{


    /**
     * IP
     */
    private String IP;
    /**
     * 手机号
     */
    private String phoneNum;
    /**
     * 验证码
     */
    private String verification;

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }





    @Override
    public String toString() {
        return "Phone{" +
                "IP='" + IP + '\'' +
                ", phoneNum='" + phoneNum + '\'' +
                ", verification='" + verification + '\'' +
                '}';
    }
}
