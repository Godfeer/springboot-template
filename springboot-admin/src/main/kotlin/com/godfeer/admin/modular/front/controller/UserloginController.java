package com.godfeer.admin.modular.front.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.godfeer.core.base.controller.BaseController;
import com.godfeer.core.util.MD5Util;
import com.godfeer.core.util.ToolUtil;
import io.swagger.annotations.*;
import io.swagger.models.HttpMethod;
import org.apache.shiro.web.session.HttpServletSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.godfeer.core.log.LogObjectHolder;
import com.godfeer.admin.modular.system.model.Userlogin;
import com.godfeer.admin.modular.front.service.IUserloginService;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 前端用户管理控制器
 *
 * @author fengshuonan
 * @Date 2018-03-22 14:09:04
 */
@Api(value = "/userlogin",description = "前端用户管理相关Api")
@Controller
@RequestMapping("/userlogin")
public class UserloginController extends BaseController {

    private String PREFIX = "/front/userlogin/";

    @Autowired
    private IUserloginService userloginService;

    /**
     * 跳转到前端用户管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "userlogin.html";
    }

    /**
     * 跳转到添加前端用户管理
     */
    @RequestMapping("/userlogin_add")
    public String userloginAdd() {
        return PREFIX + "userlogin_add.html";
    }

    /**
     * 跳转到修改前端用户管理
     */
    @RequestMapping("/userlogin_update/{userloginId}")
    public String userloginUpdate(@PathVariable Integer userloginId, Model model) {
        Userlogin userlogin = userloginService.selectById(userloginId);
        userlogin.setPassword(MD5Util.encrypt(userlogin.getPassword()));//设置密码加密
        model.addAttribute("item",userlogin);
        LogObjectHolder.me().set(userlogin);
        return PREFIX + "userlogin_edit.html";
    }

    /**
     * 获取前端用户管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return userloginService.selectList(null);
    }

    /**
     * 新增前端用户管理
     */
    @ApiOperation(value = "用户添加接口",notes = "新增用户" )
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public Object add(@RequestBody Userlogin userlogin,
                      @ApiParam(required = true,value = "验证码")
                      @RequestParam String verification, HttpServletRequest request) {
        //获取参数，手机号，用户名
        Map<String,String> varMap = new HashMap<>();
        varMap.put("username",userlogin.getUsername());
        varMap.put("phone",userlogin.getPhone());
        //判断用户是否已经注册
        boolean regOrNot = userloginService.verRegOrNot(varMap);
        if(regOrNot){
            ERROR_TIP.setMessage("该用户名或手机号已经注册！");
            return ERROR_TIP;
        }
        String ss = (String)request.getSession().getAttribute(userlogin.getPhone());
        System.out.println("Phone Math:------------------------"+ss);
        if (ss != null&&verification!=null) {
            if (ss.equals(verification)) {//校验
                System.out.println("验证码校验成功！");
                //判断必填属性
                if (userlogin.getUsername() != null && userlogin.getPassword() != null) {
                    String pass = MD5Util.encrypt(userlogin.getPassword());//密码加密
                    userlogin.setPassword(pass);
                    System.out.println("密码加密后：--------------------" + userlogin.getPassword());
                    //添加操作
                    userloginService.insert(userlogin);
                    SUCCESS_TIP.setData("");
                    return SUCCESS_TIP;
                }
            }
        }
        ERROR_TIP.setMessage("验证失败！");
        return ERROR_TIP;
    }

    /**
     * 删除前端用户管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer userloginId) {
        userloginService.deleteById(userloginId);
        return SUCCESS_TIP;
    }

    /**
     * 修改前端用户管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Userlogin userlogin) {
        userloginService.updateById(userlogin);
        return SUCCESS_TIP;
    }

    /**
     * 前端用户管理详情
     */
    @RequestMapping(value = "/detail/{userloginId}")
    @ResponseBody
    public Object detail(@PathVariable("userloginId") Integer userloginId) {
        return userloginService.selectById(userloginId);
    }

    /**
     * 用户登陆接口
     * @param userlogin
     * @return
     */
    @ApiOperation(value = "用户登陆接口")
    @RequestMapping(value = "/userlogin",method = RequestMethod.POST)
    @ResponseBody
    public Object userlogin(@RequestBody Userlogin userlogin){
        //判断非空参数
        if (userlogin.getUsername()!=null&&userlogin.getPassword()!=null){
            //根据名称检索
            Userlogin user = userloginService.selectByName(userlogin.getUsername());
            //如果该用户存在再进行密码比较
            if (user!=null){
                String pass = MD5Util.encrypt(userlogin.getPassword());//密码加密
                if (pass.equals(user.getPassword())){
                    SUCCESS_TIP.setData(user);
                    return SUCCESS_TIP;
                }
            }
        }
        return ERROR_TIP;
    }


}