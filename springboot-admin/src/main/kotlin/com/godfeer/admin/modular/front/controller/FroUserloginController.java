package com.godfeer.admin.modular.front.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.godfeer.core.base.controller.BaseController;
import com.godfeer.core.util.MD5Util;
import com.godfeer.core.util.ToolUtil;
import com.godfeer.admin.modular.front.service.IUserloginService;
import com.godfeer.admin.modular.front.service.IUserloginfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.godfeer.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.godfeer.admin.modular.system.model.FroUserlogin;
import com.godfeer.admin.modular.front.service.IFroUserloginService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户列表控制器
 *
 * @author fengshuonan
 * @Date 2018-04-01 17:12:57
 */
@Controller
@RequestMapping("/froUserlogin")
public class FroUserloginController extends BaseController {

    private String PREFIX = "/front/froUserlogin/";

    @Autowired
    private IFroUserloginService froUserloginService;


    @Autowired
    private IUserloginService userloginService;

    /**
     * 跳转到用户列表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "froUserlogin.html";
    }

    /**
     * 跳转到添加用户列表
     */
    @RequestMapping("/froUserlogin_add")
    public String froUserloginAdd() {
        return PREFIX + "froUserlogin_add.html";
    }

    /**
     * 跳转到修改用户列表
     */
    @RequestMapping("/froUserlogin_update/{froUserloginId}")
    public String froUserloginUpdate(@PathVariable Integer froUserloginId, Model model) {
        FroUserlogin froUserlogin = froUserloginService.selectById(froUserloginId);
        model.addAttribute("item",froUserlogin);
        LogObjectHolder.me().set(froUserlogin);
        return PREFIX + "froUserlogin_edit.html";
    }

    /**
     * 获取用户列表列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        if(ToolUtil.isEmpty(condition)){
            return froUserloginService.selectList(null);
        }else{
            return froUserloginService.selectListByUsernameOrPhone(condition);
        }


    }

    /**
     * 新增用户列表
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(FroUserlogin froUserlogin) {
        String pass = MD5Util.encrypt(froUserlogin.getPassword());//密码加密
        froUserlogin.setPassword(pass);
        //验证是否存在用户名或者手机号
        //获取参数，手机号，用户名
        Map<String,String> varMap = new HashMap<>();
        varMap.put("username",froUserlogin.getUsername());
        varMap.put("phone",froUserlogin.getPhone());
        //判断用户是否已经注册
        boolean regOrNot = userloginService.verRegOrNot(varMap);
        if(regOrNot){
            ERROR_TIP.setMessage("该用户名或手机号已经注册！");
            return ERROR_TIP;
        }
        froUserloginService.insert(froUserlogin);
        return SUCCESS_TIP;
    }

    /**
     * 删除用户列表
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer froUserloginId) {
        froUserloginService.deleteById(froUserloginId);
        return SUCCESS_TIP;
    }

    /**
     * 修改用户列表
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(FroUserlogin froUserlogin) {
        froUserloginService.updateById(froUserlogin);
        return SUCCESS_TIP;
    }

    /**
     * 用户列表详情
     */
    @RequestMapping(value = "/detail/{froUserloginId}")
    @ResponseBody
    public Object detail(@PathVariable("froUserloginId") Integer froUserloginId) {
        return froUserloginService.selectById(froUserloginId);
    }
}
