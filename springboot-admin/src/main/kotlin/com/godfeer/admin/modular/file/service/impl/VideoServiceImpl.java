package com.godfeer.admin.modular.file.service.impl;

import com.godfeer.admin.modular.system.model.Video;
import com.godfeer.admin.modular.system.dao.VideoMapper;
import com.godfeer.admin.modular.file.service.IVideoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
@Service
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements IVideoService {

}
