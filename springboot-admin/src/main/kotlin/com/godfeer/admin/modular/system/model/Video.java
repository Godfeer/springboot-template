package com.godfeer.admin.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
@TableName("file_video")
public class Video extends Model<Video> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 视频名
     */
    private String title;
    /**
     * 视频简介
     */
    @TableField("video_introduce")
    private String videoIntroduce;
    /**
     * 视频类型
     */
    @TableField("video_type")
    private Integer videoType;
    /**
     * 视频类别
     */
    @TableField("video_class")
    private Integer videoClass;
    /**
     * 视频下载路径
     */
    @TableField("video_url")
    private String videoUrl;
    /**
     * 图片名称
     */
    @TableField("img_title")
    private String imgTitle;
    /**
     * 图片地址
     */
    @TableField("img_url")
    private String imgUrl;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoIntroduce() {
        return videoIntroduce;
    }

    public void setVideoIntroduce(String videoIntroduce) {
        this.videoIntroduce = videoIntroduce;
    }

    public Integer getVideoType() {
        return videoType;
    }

    public void setVideoType(Integer videoType) {
        this.videoType = videoType;
    }

    public Integer getVideoClass() {
        return videoClass;
    }

    public void setVideoClass(Integer videoClass) {
        this.videoClass = videoClass;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getImgTitle() {
        return imgTitle;
    }

    public void setImgTitle(String imgTitle) {
        this.imgTitle = imgTitle;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Video{" +
        "id=" + id +
        ", title=" + title +
        ", videoIntroduce=" + videoIntroduce +
        ", videoType=" + videoType +
        ", videoClass=" + videoClass +
        ", videoUrl=" + videoUrl +
        ", imgTitle=" + imgTitle +
        ", imgUrl=" + imgUrl +
        "}";
    }
}
