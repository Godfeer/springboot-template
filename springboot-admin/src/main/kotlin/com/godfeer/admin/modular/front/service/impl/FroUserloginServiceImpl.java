package com.godfeer.admin.modular.front.service.impl;

import com.godfeer.admin.modular.system.model.FroUserlogin;
import com.godfeer.admin.modular.system.dao.FroUserloginMapper;
import com.godfeer.admin.modular.front.service.IFroUserloginService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.godfeer.admin.modular.system.model.Userlogin;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author cz123
 * @since 2018-04-01
 */
@Service
public class FroUserloginServiceImpl extends ServiceImpl<FroUserloginMapper, FroUserlogin> implements IFroUserloginService {

    @Resource
    private FroUserloginMapper froUserloginMapper;

    @Override
   public  List<FroUserlogin> selectListByUsernameOrPhone(String condition){
        return froUserloginMapper.selectListByUsernameOrPhone(condition);
    }



}
