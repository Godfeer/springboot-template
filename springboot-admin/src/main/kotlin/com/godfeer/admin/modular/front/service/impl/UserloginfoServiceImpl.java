package com.godfeer.admin.modular.front.service.impl;

import com.godfeer.admin.modular.system.model.Userloginfo;
import com.godfeer.admin.modular.system.dao.UserloginfoMapper;
import com.godfeer.admin.modular.front.service.IUserloginfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息详情表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-03-22
 */
@Service
public class UserloginfoServiceImpl extends ServiceImpl<UserloginfoMapper, Userloginfo> implements IUserloginfoService {



}
