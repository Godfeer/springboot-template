package com.godfeer.admin.modular.front.service;

import com.godfeer.admin.modular.system.model.Userlogin;
import com.baomidou.mybatisplus.service.IService;
import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author cz123
 * @since 2018-04-01
 */
public interface IUserloginService extends IService<Userlogin> {


    /**
     * 用户登陆
     * @param username
     * @return
     */
    Userlogin selectByName(String username);

    /**
     *
     * @param varMap
     * @return
     */
    boolean verRegOrNot(Map<String, String> varMap);

}
