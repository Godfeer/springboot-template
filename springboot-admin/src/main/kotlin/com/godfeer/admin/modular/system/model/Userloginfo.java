package com.godfeer.admin.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户信息详情表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-03-22
 */
@TableName("fro_userloginfo")
public class Userloginfo extends Model<Userloginfo> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private Integer userid;
    /**
     * 信用值
     */
    private Integer credit;
    /**
     * 音乐标签
     */
    private String musictag;
    /**
     * 明星标签
     */
    private String startag;
    /**
     * 我的自定义标签
     */
    private String mytag;
    /**
     * 公司名称
     */
    private String companyname;
    /**
     * 行业id
     */
    private Integer industryid;
    /**
     * 职业
     */
    private String position;
    /**
     * 累计参加次数
     */
    private Integer joincount;
    /**
     * 累计爽约次数
     */
    private Integer misscount;
    /**
     * 是否缴纳保证金标识
     */
    private Integer cautionmoneyflag;
    /**
     * 接受消息标识
     */
    private Integer receivemsg;
    /**
     * 声音提示标识
     */
    private Integer soundremind;
    /**
     * 震动提示标识
     */
    private Integer shockremind;
    /**
     * 抽奖次数
     */
    private Integer lotterynum;
    /**
     * 扩展字段
     */
    private String ext1;
    /**
     * 扩展字段
     */
    private String ext2;
    /**
     * 扩展字段
     */
    private String ext3;


    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public String getMusictag() {
        return musictag;
    }

    public void setMusictag(String musictag) {
        this.musictag = musictag;
    }

    public String getStartag() {
        return startag;
    }

    public void setStartag(String startag) {
        this.startag = startag;
    }

    public String getMytag() {
        return mytag;
    }

    public void setMytag(String mytag) {
        this.mytag = mytag;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public Integer getIndustryid() {
        return industryid;
    }

    public void setIndustryid(Integer industryid) {
        this.industryid = industryid;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getJoincount() {
        return joincount;
    }

    public void setJoincount(Integer joincount) {
        this.joincount = joincount;
    }

    public Integer getMisscount() {
        return misscount;
    }

    public void setMisscount(Integer misscount) {
        this.misscount = misscount;
    }

    public Integer getCautionmoneyflag() {
        return cautionmoneyflag;
    }

    public void setCautionmoneyflag(Integer cautionmoneyflag) {
        this.cautionmoneyflag = cautionmoneyflag;
    }

    public Integer getReceivemsg() {
        return receivemsg;
    }

    public void setReceivemsg(Integer receivemsg) {
        this.receivemsg = receivemsg;
    }

    public Integer getSoundremind() {
        return soundremind;
    }

    public void setSoundremind(Integer soundremind) {
        this.soundremind = soundremind;
    }

    public Integer getShockremind() {
        return shockremind;
    }

    public void setShockremind(Integer shockremind) {
        this.shockremind = shockremind;
    }

    public Integer getLotterynum() {
        return lotterynum;
    }

    public void setLotterynum(Integer lotterynum) {
        this.lotterynum = lotterynum;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1;
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2;
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3;
    }

    @Override
    protected Serializable pkVal() {
        return this.userid;
    }

    @Override
    public String toString() {
        return "Userloginfo{" +
        "userid=" + userid +
        ", credit=" + credit +
        ", musictag=" + musictag +
        ", startag=" + startag +
        ", mytag=" + mytag +
        ", companyname=" + companyname +
        ", industryid=" + industryid +
        ", position=" + position +
        ", joincount=" + joincount +
        ", misscount=" + misscount +
        ", cautionmoneyflag=" + cautionmoneyflag +
        ", receivemsg=" + receivemsg +
        ", soundremind=" + soundremind +
        ", shockremind=" + shockremind +
        ", lotterynum=" + lotterynum +
        ", ext1=" + ext1 +
        ", ext2=" + ext2 +
        ", ext3=" + ext3 +
        "}";
    }
}
