package com.godfeer.admin.modular.front.service;

import com.godfeer.admin.modular.system.model.FroUserlogin;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author cz123
 * @since 2018-04-01
 */
public interface IFroUserloginService extends IService<FroUserlogin> {

    List<FroUserlogin> selectListByUsernameOrPhone(String condition);

}
