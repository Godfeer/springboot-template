package com.godfeer.admin.modular.system.dao;

import com.godfeer.admin.modular.system.model.Userlogin;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author cz123
 * @since 2018-04-01
 */
public interface UserloginMapper extends BaseMapper<Userlogin> {


    /**
     * 前端用户登陆
     * @param username
     * @return
     */
    Userlogin selectByName(String username);


    /**
     * 验证是否注册
     * @param varMap
     * @return
     */
    Userlogin verRegOrNot(Map<String, String> varMap);


}
