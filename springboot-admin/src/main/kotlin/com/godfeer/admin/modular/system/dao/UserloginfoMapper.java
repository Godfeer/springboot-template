package com.godfeer.admin.modular.system.dao;

import com.godfeer.admin.modular.system.model.Userloginfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户信息详情表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-03-22
 */
public interface UserloginfoMapper extends BaseMapper<Userloginfo> {

}
