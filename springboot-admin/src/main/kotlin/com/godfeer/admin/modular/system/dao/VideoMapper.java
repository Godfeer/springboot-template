package com.godfeer.admin.modular.system.dao;

import com.godfeer.admin.modular.system.model.Video;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
public interface VideoMapper extends BaseMapper<Video> {

}
