package com.godfeer.admin.modular.file.service.impl;

import com.godfeer.admin.modular.system.model.Upload;
import com.godfeer.admin.modular.system.dao.UploadMapper;
import com.godfeer.admin.modular.file.service.IUploadService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UploadServiceImpl extends ServiceImpl<UploadMapper, Upload> implements IUploadService {

    @Resource
    private UploadMapper uploadMapper;

    public void deleteFileByName(Upload upload){
        uploadMapper.deleteFileByName(upload);
    }
}
