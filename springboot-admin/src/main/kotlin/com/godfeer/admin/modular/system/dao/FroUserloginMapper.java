package com.godfeer.admin.modular.system.dao;

import com.godfeer.admin.modular.system.model.FroUserlogin;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author cz123
 * @since 2018-04-01
 */
public interface FroUserloginMapper extends BaseMapper<FroUserlogin> {

    List<FroUserlogin> selectListByUsernameOrPhone(String condtion);

}
