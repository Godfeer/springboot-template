package com.godfeer.admin.modular.file.service;

import com.godfeer.admin.modular.system.model.Upload;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cz123
 * @since 2018-04-04
 */
public interface IUploadService extends IService<Upload> {

    void deleteFileByName(Upload upload);

}
