package com.godfeer.admin.modular.file.service;

import com.godfeer.admin.modular.system.model.Video;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
public interface IVideoService extends IService<Video> {



}
