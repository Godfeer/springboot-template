package com.godfeer.admin

import org.apache.log4j.Logger
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.transaction.annotation.EnableTransactionManagement

@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = arrayOf("com.godfeer.admin"))
class SpringbootAdminApplication


fun main(args: Array<String>) {
     val logger = Logger.getLogger(SpringbootAdminApplication::class.java)
    runApplication<SpringbootAdminApplication>(*args)
    logger.info("========================================================================================================================\n")
    logger.info("admin  后端服务器 success!")
    logger.info("========================================================================================================================\n")
}
