package com.godfeer.admin

import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

/**
 * subor Web程序启动类
 *
 * @author fengshuonan
 * @date 2017-05-21 9:43
 */
class SpringbootServletInitializer : SpringBootServletInitializer() {

    override fun configure(builder: SpringApplicationBuilder): SpringApplicationBuilder {
        return builder.sources(SpringbootServletInitializer::class.java)
    }
}