# springboot-template

#### 项目介绍
后端项目通用模板

- 整合了 jpa，redis，shiro。
- 使用Gradle 对项目进行模块化管理，提高项目的易开发性、扩展性。

## 依赖

- Gradle 4.1
- Java 8
- MySQL 5.7
- Redis 3.2.9

## 工程说明
> springboot-admin 管理系统模块

> springboot-core 公共代码

> springboot-generator web自动模板创建模块

> springboot-parent 依赖模块

> springboot-rest 独立接口模块

